# Example Authenticated Workload

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Deployment configuration for the Hello World app with additional configuration for the service mesh and authentication gateway

## Table of Contents

- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Usage

Create namespace

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: example-authenticated-workload
  namespace: example-authenticated-workload
```

Create secrets

```sh
redis_password=$(head -c 32 /dev/urandom | base64 | head -c 16)
cookie_secret=$(head -c 32 /dev/urandom | base64 | head -c 32)

# Redis password
k create secret generic -n example-authenticated-workload redis-password --from-literal=redis-password=$redis_password

# Retrieve client id and secret from Keycloak
k create secret generic -n example-authenticated-workload authn \
  --from-literal=cookie-secret=$cookie_secret \
  --from-literal=client-id=$client_id \
  --from-literal=client-secret=$client_secret
```

Deploy using Fleet

```yaml
kind: GitRepo
apiVersion: fleet.cattle.io/v1alpha1
metadata:
  name: example-authenticated-workload
  namespace: fleet-local
spec:
  repo: https://gitlab.com/noroff-accelerate/cloud/projects/aks-microservices/aks-example-authenticated-workload
  branch: master
  targetNamespace: example-authenticated-workload
  paths:
    - sessions-redis
    - oauth2-proxy
    - hello-world
```

## Maintainers

[Greg Linklater (@EternalDeiwos)](https://gitlab.com/EternalDeiwos)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2020 Noroff Accelerate AS
